import './App.css';
import './css/report.css'
import ReactToPrint from 'react-to-print';
import Report from './component/Report';
import React,{useRef, useState} from 'react';

function App() {

  const [isPrint,SetIsPrint]=useState()
  
  const componentRef=useRef();
  return (
    <div className="App">
      <ReactToPrint
      onAfterPrint={()=>{SetIsPrint(false)}}
      onBeforePrint={()=>{}}
      onBeforeGetContent={()=>{
        return new Promise((resolve)=>{
          SetIsPrint(true)
          setTimeout(()=>{
            resolve(true)
          },200)
        })
        }}
      trigger={()=><button> print this out!</button>}
      content={()=>componentRef.current}
      />
      <Report isPrint={isPrint} ref={componentRef}></Report>
    </div>
  );
}

export default App;
