//import { getValue } from '@testing-library/user-event/dist/utils';
//import React,{forwardRef,useState} from 'react'
//import DatePicker from "react-datepicker";
//import "react-datepicker/dist/react-datepicker.css";
//import '../css/report.css'
//import moment from 'moment';

import React, { forwardRef, useState } from 'react'
import DatePicker from 'react-date-picker';
import moment from 'moment';


const ComponentInput = (props) => {
    const { data, defaults } = props
    return (
        <>
            {!defaults.isPrint ? (<input
                placeholder={data.ph}
                value={defaults.content[data.name]}
                name={data.name}
                onChange={defaults.setOnChange}
            />) : defaults.content[data.name]
            }
        </>
    )
}


const ComponentDatePicker = (props) => {
    const { data, defaults, type } = props
    return (
        <>
            {!defaults.isPrint ? (<DatePicker
                value={defaults.content[data.name] == null ?
                    new Date() :
                    new Date(defaults.content[data.name])}
                onChange={(date) => {
                    defaults.setContent({
                        ...defaults.content,
                        [data.name]: moment(date).format('YYYY/MM/DD')
                    })
                }}
            />) : moment(defaults.content[data.name]).format(type == null ? ', [ngày] DD [tháng] MM [năm] YYYY' : 'DD / MM / YYYY')
            }
        </>
    )
}





export default forwardRef(function Report(props,ref){

    const {isPrint}=props
    const [content, setContent]=useState({})
    const setOnChange =(e) =>{
        const name=e.target.name
        const value=e.target.value
        setContent({
            ...content,
            [name]:value
        })
    }
    const defaults={isPrint,content,setOnChange,setContent}


    return(
        <div
        className='report'
        ref={ref}>
            <style type="text/css" media="print">
            {"@page { size:landscape; }"}
            </style>
            <div className='report__heading'>
                <div className='report__heading_1 ta-c fs-10'>Phụ lục III-3</div>
                <div className='report__heading_2 ta-c fs-10'>(Ban hành kèm theo Thông tư số 01/2021/TT-BKHĐT ngày 16 tháng 03 năm 2021 của Bộ trưởng Bộ Kế hoạch và Đầu tư)</div>
                <div className='report__heading_3'>
                    <div className='report__heading_3_1 ta-c fs-10'>
                    <ComponentInput data=
                    {{
                        ph:'Nhập hộ kinh doanh',
                        name:'report__heading_3_1_TenHoKinhDoanh'
                    }}
                        defaults={defaults}
                    />
                    <br></br>
                    -------
                    </div>
                    <div className='report__heading_3_2 ta-c fs-10'>
                    CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM
                    <br></br>
                    Độc lập - Tự do - Hạnh phúc 
                    <br></br>
                    ---------------
                    </div>
                </div>
                <div className='report__heading_4 ta-c fs-10'>
                    <div className='report__heading_4_1 ta-c fs-10'>
                    Số:
                    <ComponentInput data=
                    {{
                        ph:'Nhập số',
                        name:'report__heading_4_1_So'
                    }}
                        defaults={defaults}
                    />
                    </div>
                    <div className='report__heading_4_2 ta-c fs-10'>
                        <ComponentInput data=
                            {{
                                ph:'Nhập:',
                                name:'report__heading_4_2_1_tt'
                            }}
                                defaults={defaults}
                        />
                        <ComponentDatePicker
                            data={{
                                name:'report__heading_4_2_2_Ngay'
                            }}
                                defaults={defaults}        
                        />
                    </div>
                </div>
            </div>
            <br></br>
            <div className='report__body'>
                <div className='report__body__heading'>
                    <div className='report__body__heading_1 ta-c fs-10 fw-b'>
                        THÔNG BÁO
                    </div>
                    <div className='report__body__heading_2 ta-c fs-10 fw-b'>
                    Thay đổi chủ hộ kinh doanh
                    </div>   
                    <div className='report__body__heading_3 ta-c fs-10'>
                    Kính gửi: Phòng Tài chính - Kế hoạch    <ComponentInput data=
                            {{
                                ph: 'Nhập:',
                                name:'report__body__heading_3'
                            }}
                                defaults={defaults}
                        />
                    </div>           
                </div>
                <div className='report__body__content ta-l fs-10'>
                    <div className='report__body__content_1'>
                        Tên hộ kinh doanh <span className='fs-i'>(ghi bằng chữ in hoa)</span>: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_1_tenHoKinhDoanh'
                                }}
                                defaults={defaults}
                            />
                    </div>
                    <div className='report__body__content_2'>
                        Số Giấy chứng nhận đăng ký hộ kinh doanh: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_2_soGiayChungNhanHoKinhDoan'
                                }}
                                defaults={defaults}
                            />
                    </div>
                    <div className='report__body__content_3'>
                        Cấp lần đầu ngày: <ComponentDatePicker
                                data={{
                                    name: 'report__body__content_3_capLanDau'
                                }}
                                defaults={defaults}
                                type={1}
                            /> tại: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_3_tai'
                                }}
                                defaults={defaults}
                            />
                    </div>
                    <div className='report__body__content_4'>
                        Thay đổi lần cuối ngày: <ComponentDatePicker
                                data={{
                                    name: 'report__body__content_4_thayDoiLanCuoi'
                                }}
                                defaults={defaults}
                                type={1}
                            /> tại: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_4_tai'
                                }}
                                defaults={defaults}
                            />
                    </div>
                    <div className='report__body__content_5'>
                        Địa chỉ trụ sở hộ kinh doanh: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_5_diaChiChuHo'
                                }}
                                defaults={defaults}
                            />
          
                    </div>
                    <div className='report__body__content_6'>
                        <div className='report__body__content_6_1 w-50vw ta-l'>
                            Điện thoại  <span className='fs-i'>(nếu có)</span>: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_6_1_soDienThoai'
                                }}
                                defaults={defaults}
                            />
           
                        </div>
                        <div className='report__body__content_6_2'>
                            Fax <span className='fs-i'>(nếu có)</span>: <ComponentInput
                                    data={{
                                        ph: 'Nhập',
                                        name: 'report__body__content_6_2_fax'
                                    }}
                                    defaults={defaults}
                                />
           
                        </div>          
                    </div>
                    <div className='report__body__content_7'>
                        <div className='report__body__content_7_1'>
                            Email <span className='fs-i'>(nếu có)</span>: <ComponentInput
                                    data={{
                                        ph: 'Nhập',
                                        name: 'report__body__content_7_1_email'
                                    }}
                                    defaults={defaults}
                                />
                        </div>
                        <div className='report__body__content_7_2'>
                            Website <span className='fs-i'>(nếu có)</span>: <ComponentInput
                                    data={{
                                        ph: 'Nhập',
                                        name: 'report__body__content_7_2_web'
                                    }}
                                    defaults={defaults}
                                />
                        </div>          
                    </div>
                    <div className='report__body__content_8 fw-b'>
                        Đăng ký thay đổi chủ hộ kinh doanh với các nội dung sau:
                    </div>
                    <div className='report__body__content_9 '>
                        Thay đổi chủ hộ kinh doanh do <span className='fs-i' >(đánh dấu X vào ô thích hợp)</span>:
                    </div>
                    <div className='report__body__content_10 d-flex '>
                        <div className='report__body__content_10_1 d-flex'>
                            Thành viên hộ gia đình ủy quyền cho người khác là chủ hộ kinh doanh
                            <br></br>
                            Tặng cho hộ kinh doanh
                            <br></br>
                            Bán hộ kinh doanh
                            <br></br>
                            Thừa kế hộ kinh doanh
                        </div>
                        <div className='report__body__content_10_2 d-flex'>
                            <div class=" form-check">
                                <input class="form-check-input" type="checkbox" value="" />
                            </div>
                            <div class=" form-check">
                                <input class="form-check-input" type="checkbox" value="" />
                            </div>
                            <div class=" form-check">
                                <input class="form-check-input" type="checkbox" value="" />
                            </div>
                            <div class=" form-check">
                                <input class="form-check-input" type="checkbox" value="" />
                            </div>
                        </div>
                    </div>
                    <div className='report__body__content_11 fw-b'>
                        1. Người tặng cho/Người bán/Người chết/Chủ hộ kinh doanh trước khi thay đổi:
                    </div>
                    <div className='report__body__content_12 d-flex flex-row '>
                        <div className='report__body__content_12_1 w-40vw ta-l fs-10'>
                            Họ và tên <span className='fs-i'>(ghi bằng chữ in hoa)</span>: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_12_1_hoVaTen'
                                }}
                                defaults={defaults}
                            />
                        </div>
                        <div className='report__body__content_12_2 w-40vw ta-l fs-10'>
                            Giớí tính:  <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_12_2_gioiTinh'
                                }}
                                defaults={defaults}
                            />
                        </div>
                    </div>
                    <div className='report__body__content_13 d-flex flex-row '>
                        <div className='report__body__content_13_1_sinhNgay w-20vw ta-l fs-10'>
                            Sinh ngày: <ComponentDatePicker
                                data={{
                                    name: 'report__body__content_13_1_sinhNgay'
                                }}
                                defaults={defaults}
                                type={1}
                            />
                        </div>
                        <div className='report__body__content_13_2 w-20vw ta-l fs-10'>
                            Dân tộc:  <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_13_2_danToc'
                                }}
                                defaults={defaults}
                            />
                        </div>
                        <div className='report__body__content_13_3 w-20vw ta-l fs-10'>
                            Quốc tịch:  <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_13_3_quocTich'
                                }}
                                defaults={defaults}
                            />
                        </div>
                    </div>
                    <div className='report__body__content_14 '>
                        Loại giấy tờ pháp lý của cá nhân:
                        <br></br>
                        <div className='report__body__content_14_1 d-flex flex-row'>
                            <div className='d-flex flex-row w-40vw'>
                                <div class=" form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                </div>
                                Chứng minh nhân dân
                            </div>
                            <div className='d-flex flex-row'>
                                <div class=" form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                </div>
                                Căn cước công dân
                            </div>
                        </div>

                        <div className='report__body__content_14_2 d-flex flex-row'>
                            <div className='d-flex flex-row w-40vw'>
                                <div class=" form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                </div>
                                Hộ chiếu
                            </div>
                            <div className='d-flex flex-row'>
                                <div class=" form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                </div>
                                Loại khác <span className='fs-i'>(ghi rõ)</span>: <ComponentInput
                                    data={{
                                        ph: 'Nhập',
                                        name: 'report__body__content_14_2_loaiKhac'
                                    }}
                                    defaults={defaults}
                                />
                            </div>
                        </div>

                    </div>

                    <div className='report__body__content_15 '>
                        Số giấy tờ pháp lý của cá nhân:
                        <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_15_soGiayToPhapLyCaNhan'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_16 d-flex flex-row '>

                        <div className='report__body__content_16_1 w-15vw d-flex flex-row  ta-l fs-10'>
                            Ngày cấp: <ComponentDatePicker
                                data={{
                                    name: 'report__body__content_16_1_ngayCap'
                                }}
                                defaults={defaults}
                                type={1}
                            />
                        </div>
                        <div className='ms-2 report__body__content_16_2 w-15vw d-flex flex-row ta-l fs-10'>
                            Nơi cấp: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_16_2_noiCap'
                                }}
                                defaults={defaults}
                            />
                        </div>
                        <div className='ms-2 report__body__content_16_3 w-20vw d-flex flex-row  ta-l fs-10'>
                            Ngày hết hạn <span className='fs-i'>(nếu có)</span>: <ComponentDatePicker
                                data={{
                                    name: 'report__body__content_16_3_ngayHetHan'
                                }}
                                defaults={defaults}
                                type={1}
                            />
                        </div>
                    </div>
                    <div className='report__body__content_17'>
                        Địa chỉ thường trú: <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_17_diaChiThuongTru'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_18'>
                        Số nhà, ngách, hẻm, ngõ, đường phố/tổ/xóm/ấp/thôn: <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_18_soNha'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_19'>
                        Xã/Phường/Thị trấn:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_19_xaPhuong'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_20'>
                        Quận/Huyện/Thị xã/Thành phố thuộc tỉnh:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_20_quanHuyen'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_21'>
                        Tỉnh/Thành phố:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_21_tinhThanhPho'
                            }}
                            defaults={defaults}
                        />
                    </div>


                    <div className='report__body__content_22'>
                        Địa chỉ liên lạc: <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_22_diaChiLienLac'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_23'>
                        Số nhà, ngách, hẻm, ngõ, đường phố/tổ/xóm/ấp/thôn: <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_23_soNha'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_24'>
                        Xã/Phường/Thị trấn:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_24_xaPhuong'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_25'>
                        Quận/Huyện/Thị xã/Thành phố thuộc tỉnh:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_25_quanHuyen'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_26'>
                        Tỉnh/Thành phố:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_26_tinhThanhPho'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_27 d-flex'>
                        <div className='report__body__content_27_1 w-50vw ta-l'>
                            Điện thoại <span className='fs-i'>(nếu có)</span>: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_27_1_soDienThoai'
                                }}
                                defaults={defaults}
                            />
                        </div>
                        <div className='report__body__content_27_2 w-50vw ta-l'>
                            Email <span className='fs-i'>(nếu có)</span>:  <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_27_2_email'
                                }}
                                defaults={defaults}
                            />
                        </div>
                    </div>

                    <div className='report__body__content_28 fw-b'>
                        2. Người được tặng cho/Người mua/Người thừa kế/Chủ hộ kinh doanh sau khi thay đổi:
                    </div>
                    <div className='report__body__content_29 d-flex flex-row '>
                        <div className='report__body__content_29_1 w-40vw ta-l fs-10'>
                            Họ và tên (ghi bằng chữ in hoa): <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_29_1_hoVaTen'
                                }}
                                defaults={defaults}
                            />
                        </div>
                        <div className='report__body__content_29_2 w-40vw ta-l fs-10'>
                            Giớí tính:  <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_29_2_gioiTinh'
                                }}
                                defaults={defaults}
                            />
                        </div>
                    </div>
                    <div className='report__body__content_30 d-flex flex-row '>
                        <div className='report__body__content_30_1_sinhNgay w-20vw ta-l fs-10'>
                            Sinh ngày: <ComponentDatePicker
                                data={{
                                    name: 'report__body__content_30_1_sinhNgay'
                                }}
                                defaults={defaults}
                                type={1}
                            />
                        </div>
                        <div className='report__body__content_30_2 w-20vw ta-l fs-10'>
                            Dân tộc:  <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_30_2_danToc'
                                }}
                                defaults={defaults}
                            />
                        </div>
                        <div className='report__body__content_30_3 w-20vw ta-l fs-10'>
                            Quốc tịch:  <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_30_3_quocTich'
                                }}
                                defaults={defaults}
                            />
                        </div>
                    </div>
                    <div className='report__body__content_31 '>
                        Loại giấy tờ pháp lý của cá nhân:
                        <br></br>
                        <div className='report__body__content_31_1 d-flex flex-row'>
                            <div className='d-flex flex-row w-40vw'>
                                <div class=" form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                </div>
                                Chứng minh nhân dân
                            </div>
                            <div className='d-flex flex-row'>
                                <div class=" form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                </div>
                                Căn cước công dân
                            </div>
                        </div>

                        <div className='report__body__content_31_2 d-flex flex-row'>
                            <div className='d-flex flex-row w-40vw'>
                                <div class=" form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                </div>
                                Hộ chiếu
                            </div>
                            <div className='d-flex flex-row'>
                                <div class=" form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                </div>
                                Loại khác (ghi rõ): <ComponentInput
                                    data={{
                                        ph: 'Nhập',
                                        name: 'report__body__content_31_2_loaiKhac'
                                    }}
                                    defaults={defaults}
                                />
                            </div>
                        </div>

                    </div>

                    <div className='report__body__content_32 '>
                        Số giấy tờ pháp lý của cá nhân:
                        <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_32_soGiayToPhapLyCaNhan'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_33 d-flex flex-row '>

                        <div className='report__body__content_33_1 w-15vw d-flex flex-row  ta-l fs-10'>
                            Ngày cấp: <ComponentDatePicker
                                data={{
                                    name: 'report__body__content_33_1_ngayCap'
                                }}
                                defaults={defaults}
                                type={1}
                            />
                        </div>
                        <div className='ms-2 report__body__content_33_2 w-15vw d-flex flex-row ta-l fs-10'>
                            Nơi cấp: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_33_2_noiCap'
                                }}
                                defaults={defaults}
                            />
                        </div>
                        <div className='ms-2 report__body__content_16_3 w-20vw d-flex flex-row  ta-l fs-10'>
                            Ngày hết hạn <span className='fs-i'>(nếu có)</span>: <ComponentDatePicker
                                data={{
                                    name: 'report__body__content_33_3_ngayHetHan'
                                }}
                                defaults={defaults}
                                type={1}
                            />
                        </div>
                    </div>
                    <div className='report__body__content_34'>
                        Địa chỉ thường trú: <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_34_diaChiThuongTru'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_35'>
                        Số nhà, ngách, hẻm, ngõ, đường phố/tổ/xóm/ấp/thôn: <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_35_soNha'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_36'>
                        Xã/Phường/Thị trấn:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_36_xaPhuong'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_37'>
                        Quận/Huyện/Thị xã/Thành phố thuộc tỉnh:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_37_quanHuyen'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_38'>
                        Tỉnh/Thành phố:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_38_tinhThanhPho'
                            }}
                            defaults={defaults}
                        />
                    </div>


                    <div className='report__body__content_39'>
                        Địa chỉ liên lạc: <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_39_diaChiLienLac'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_40'>
                        Số nhà, ngách, hẻm, ngõ, đường phố/tổ/xóm/ấp/thôn: <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_40_soNha'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_41'>
                        Xã/Phường/Thị trấn:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_41_xaPhuong'
                            }}
                            defaults={defaults}
                        />
                    </div>

                    <div className='report__body__content_42'>
                        Quận/Huyện/Thị xã/Thành phố thuộc tỉnh:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_42_quanHuyen'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_43'>
                        Tỉnh/Thành phố:  <ComponentInput
                            data={{
                                ph: 'Nhập',
                                name: 'report__body__content_43_tinhThanhPho'
                            }}
                            defaults={defaults}
                        />
                    </div>
                    <div className='report__body__content_44 d-flex'>
                        <div className='report__body__content_44_1 w-50vw ta-l'>
                            Điện thoại <span className='fs-i'>(nếu có)</span>: <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_44_1_soDienThoai'
                                }}
                                defaults={defaults}
                            />
                        </div>
                        <div className='report__body__content_44_2 w-50vw ta-l'>
                            Email <span className='fs-i'>(nếu có)</span>:  <ComponentInput
                                data={{
                                    ph: 'Nhập',
                                    name: 'report__body__content_44_2_email'
                                }}
                                defaults={defaults}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className='report__footer fs-10'>
                <div className='report__footer_1 ta-l fs-10'>
                    Chúng tôi cam kết về tính hợp pháp, chính xác, trung thực và chịu trách nhiệm trước pháp luật về nội dung của Thông báo này.
                </div>
                <br></br>
                <div className='report__footer_2 ta-c d-flex flex-row'>
                    <div className='report__footer_2_1 w-30vw'>
                        <span className="fw-b fs-10">
                            NGƯỜI ĐƯỢC TẶNG CHO/NGƯỜI MUA/ NGƯỜI THỪA KẾ/CHỦ HỘ KINH DOANH SAU KHI THAY ĐỔI
                        </span>
                        <br></br>
                        <span className='fs-i fs-10'>(Ký và ghi họ tên)<sup>1</sup></span>
                    </div>
                    <div className='report__footer_2_2 w-50vw'>
                        <span className="fw-b fs-10 ">
                            CHỦ HỘ KINH DOANH TRƯỚC KHI THAY ĐỔI
                        </span>
                        <br></br>
                        <span className='fs-i fs-10'>
                            (Ký và ghi họ tên)<sup>2</sup>
                        </span>
                    </div>

                </div>
                <br></br>
                <br></br>
                <br></br>

                <div className='report__footer_3 ta-l fs-10'>
                    ___________________
                </div>
                <div className='report__footer_4 ta-l fs-10'>
                    <sup>1</sup> Người được tặng cho/Người mua/Chủ hộ kinh doanh sau khi thay đổi ký trực tiếp vào phần này.
                    <br></br>
                    <sup>2</sup> Không có phần này trong trường hợp thay đổi chủ hộ kinh doanh do thừa kế. Trong các trường hợp khác, chủ hộ kinh doanh trước khi thay đổi ký trực tiếp vào phần này.
                </div>
            </div>
        </div>
    )
})